Vagrant + Docker + Serf のサンプル
=================================

1. VagrantのDocker providerで下記サーバを起動

    - ウェブサーバ Nginx (web:80)
    - ロードバランサ HAProxy (lb:80,9999)
    - データベース Redis (db:6379) を起動

2. アプリケーションサーバ(app[0-2]:8080)を起動
3. アプリケーションサーバは経由(app[0-2]:7946)でロードバランサ(lb:7946)に自動的に登録される

というサンプル。

        localhost(:8080,:9999,:6379) -> web(:80) -> lb(:80,:9999,:7946) <-> app[0-2](:8080,:7946) -> db(:6379)
        │                                           │                                                │
        └-------------------------------------------┴ -----------------------------------------------┘

## Usage

1. ウェブサーバ、ロードバランサ、データベース起動

        $ vagrant up --no-parallel --provider=docker

2. アプリケーションサーバー起動

        $ vagrant up app0 app1 app2 --provider=docker

3. テストデータ投入

        $ curl -v -H 'Accept: application/json' -H 'Content-Type: application/json; charset=utf-8' -X POST \
            -d '"HOGE"' \
            localhost:8080/hoge
        $ curl -v -H 'Accept: application/json' -H 'Content-Type: application/json; charset=utf-8' -X POST \
            -d '1' \
            localhost:8080/fuga
        $ curl -v -H 'Accept: application/json' -H 'Content-Type: application/json; charset=utf-8' -X POST \
            -d '{"foo":{"email":["taro@example.com","yamada@example.com"],"id":1,"name":"taro"},"fuga":1,"hoge":"HOGE","piyo":["aaa  bbb  ccc","bbb","ccc"]}' \
            localhost:8080/piyo

4. 確認

    * ロードバランサにアプリケーションサーバが登録されていることを確認

            $ curl 'localhost:9999/;csv' | grep -e '^http-in,app[0-9],'

    * クライアントからアプリケーションサーバを通してデータが取得できることを確認

            $ curl -v -H 'Accept: application/json' 'localhost:8080/?i'
            $ curl -v -H 'Accept: application/json' 'localhost:8080/hoge?i'
            $ curl -v -H 'Accept: application/json' 'localhost:8080/fuga?i'
            $ curl -v -H 'Accept: application/json' 'localhost:8080/piyo?i'

    * データベースにデータが登録されれていることを確認

            $ nc localhost 6379
            redis> KEYS *
            ...
            redis> GET <KEY>
            ...

5. 削除

        $ vagrant destroy -f

