#!/bin/sh

/usr/sbin/sshd -D &

export GOPATH=`dirname $0`/.go
cd `dirname $0`/app
go get github.com/garyburd/redigo/redis
go run *.go -host=$DB_PORT_6379_TCP_ADDR:$DB_PORT_6379_TCP_PORT -port=8080 redis &

/usr/bin/serf agent -role app -join $LB_PORT_80_TCP_ADDR -node $SERF_NODE_NAME
