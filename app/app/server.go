package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"./kvs"
)

type HttpServer interface {
	Serve(int) error
}

func NewKvsHttpServer(kvs kvs.KVS) HttpServer {
	return &kvsHttpServer{kvs: kvs}
}

type kvsHttpServer struct {
	kvs kvs.KVS
}

func (h *kvsHttpServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/" {
		h.all(w, r)
		return
	}

	switch key := strings.TrimPrefix(r.URL.Path, "/"); r.Method {
	case "GET":
		h.get(w, r, key)
	case "POST":
		h.set(w, r, key)
	case "DELETE":
		h.delete(w, r, key)
	default:
		http.NotFound(w, r)
	}
}

func (h *kvsHttpServer) get(w http.ResponseWriter, r *http.Request, key string) {
	var v interface{}
	ok, err := h.kvs.Exists(key)
	if err != nil {
		write(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	var st int = http.StatusNotFound
	if ok {
		v, err = h.kvs.Get(key)
		if err != nil {
			write(w, r, err.Error(), http.StatusInternalServerError)
			return
		}
		st = http.StatusOK
	}
	write(w, r, map[string]interface{}{key: v}, st)
}

func (h *kvsHttpServer) set(w http.ResponseWriter, r *http.Request, key string) {
	blob, err := ioutil.ReadAll(r.Body)
	if err != nil {
		write(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	var value interface{}
	err = json.Unmarshal(blob, &value)
	if err != nil {
		write(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	err = h.kvs.Set(key, value)
	if err != nil {
		write(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	write(w, r, string(blob), http.StatusCreated)
}

func (h *kvsHttpServer) delete(w http.ResponseWriter, r *http.Request, key string) {
	err := h.kvs.Delete(key)
	if err != nil {
		write(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	write(w, r, "", http.StatusNoContent)
}

func (h *kvsHttpServer) all(w http.ResponseWriter, r *http.Request) {
	all, err := h.kvs.All()
	if err != nil {
		write(w, r, err.Error(), http.StatusInternalServerError)
		return
	}
	write(w, r, all, http.StatusOK)
}

func write(w http.ResponseWriter, r *http.Request, data interface{}, code int) {
	ctype := "text/plain; charset=utf-8"
	accept := r.Header.Get("Accept")
	if strings.Contains(accept, "application/json") {
		ctype = "application/json; charset=utf-8"
	}

	var blob []byte
	var indent bool
	if _, ok := (r.URL.Query())["i"]; ok {
		indent = true
	}
	if indent {
		blob, _ = json.MarshalIndent(data, "", " ")
	} else {
		blob, _ = json.Marshal(data)
	}

	w.Header().Set("Content-Type", ctype)
	w.WriteHeader(code)
	w.Write(blob)
}

func (h *kvsHttpServer) Serve(port int) error {
	log.Printf("Listening at http://*:%d", port)
	return http.ListenAndServe(":"+strconv.Itoa(port), h)
}
