package kvs

import (
	"encoding/json"

	redigo "github.com/garyburd/redigo/redis"
)

const (
	Retry = 3
)

type redis struct {
	conn      redigo.Conn
	host      string
	reconnect bool
}

func NewRedis(host string, reconnect bool) (*redis, error) {
	conn, err := redigo.Dial("tcp", host)
	return &redis{conn: conn, host: host, reconnect: reconnect}, err
}

func (r *redis) Get(key string) (interface{}, error) {
	jsonStr, err := redigo.String(r.do("GET", key))
	if err != nil {
		return nil, err
	}
	var data interface{}
	json.Unmarshal([]byte(jsonStr), &data)
	return data, nil
}

func (r *redis) Set(key string, value interface{}) error {
	blob, _ := json.Marshal(value)
	_, err := r.do("SET", key, string(blob))
	return err
}

func (r *redis) Delete(key string) error {
	_, err := r.do("DEL", key)
	return err
}

func (r *redis) All() (map[string]interface{}, error) {
	values, err := redigo.Strings(redigo.Values(r.do("KEYS", "*")))
	if err != nil {
		return nil, err
	}
	all := make(map[string]interface{})
	if len(values) == 0 {
		return all, nil
	}
	for _, key := range values {
		v, err := redigo.Bytes(r.do("GET", key))
		if err != nil {
			return nil, err
		}
		var data interface{}
		err = json.Unmarshal(v, &data)
		if err != nil {
			return nil, err
		}
		all[key] = data
	}
	return all, nil
}

func (r *redis) Exists(key string) (bool, error) {
	ok, err := redigo.Bool(r.do("EXISTS", key))
	return ok, err
}

func (r *redis) withReconnect(f func() error) (err error) {
	for t := Retry; t > 0; t-- {
		if err = f(); err != nil {
			r.conn.Close()
			var conn redigo.Conn
			conn, err = redigo.Dial("tcp", r.host)
			if err != nil {
				return err
			}
			r.conn = conn
		} else {
			break
		}
	}
	return err
}

func (r *redis) do(cmd string, args ...interface{}) (v interface{}, err error) {
	if r.reconnect {
		err = r.withReconnect(func() error {
			var err error
			v, err = r.conn.Do(cmd, args...)
			return err
		})
	} else {
		v, err = r.conn.Do(cmd, args...)
	}
	return
}
