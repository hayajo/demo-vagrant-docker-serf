package kvs

import "log"

type KVS interface {
	Get(string) (interface{}, error)
	Set(string, interface{}) error
	Delete(string) error
	All() (map[string]interface{}, error)
	Exists(string) (bool, error)
}

func CreateKvs(t string, opts ...interface{}) KVS {
	var kvs KVS
	switch t {
	case "redis":
		host := opts[0].(string)
		var err error
		kvs, err = NewRedis(host, true)
		if err != nil {
			log.Fatal(err)
		}
	case "memcached":
		log.Fatal("Not yet implemented")
	case "memory":
		kvs = NewMemory()
	default:
		log.Fatalf("Invalid kvs: %s", t)
	}
	return kvs
}
