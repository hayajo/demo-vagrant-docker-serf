package kvs

import "sync"

type memory struct {
	storage map[string]interface{}
	mu      sync.Mutex
}

func NewMemory() *memory {
	return &memory{storage: make(map[string]interface{})}
}

func (m *memory) Get(key string) (interface{}, error) {
	return m.storage[key], nil
}
func (m *memory) Set(key string, value interface{}) error {
	m.mu.Lock()
	defer func() {
		m.mu.Unlock()
	}()
	m.storage[key] = value
	return nil
}
func (m *memory) Delete(key string) error {
	m.mu.Lock()
	defer func() {
		m.mu.Unlock()
	}()
	delete(m.storage, key)
	return nil
}
func (m *memory) All() (map[string]interface{}, error) {
	return m.storage, nil
}
func (m *memory) Exists(key string) (bool, error) {
	_, ok := m.storage[key]
	return ok, nil
}
