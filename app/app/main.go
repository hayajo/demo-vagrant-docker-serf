package main

import (
	"flag"
	"log"

	"./kvs"
)

func main() {
	var (
		host = flag.String("host", "", "kvs host")
		port = flag.Int("port", 8080, "httpd port")
	)
	flag.Parse()

	store := "memory"
	if len(flag.Args()) > 0 {
		store = flag.Arg(0)
	}

	kvs := kvs.CreateKvs(store, *host)

	s := NewKvsHttpServer(kvs)
	err := s.Serve(*port)
	if err != nil {
		log.Fatal(err)
	}
}
