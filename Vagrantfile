# -*- mode: ruby -*-
# vi: set ft=ruby :

HTTP_PORT=8080
REDIS_PORT=6379
HAPROXY_PORT=9999

Vagrant.configure("2") do |config|

  config.vm.define :lb do |lb|
    lb.vm.provider "docker" do |d|
      d.build_dir = "./lb"
      d.name = "lb"
    end
    lb.vm.network :forwarded_port, guest: 9999, host: HAPROXY_PORT
  end

  config.vm.define :db do |db|
    db.vm.provider "docker" do |d|
      d.image = "dockerfile/redis"
      d.name = "db"
    end
    db.vm.network :forwarded_port, guest: REDIS_PORT, host: REDIS_PORT
  end

  config.vm.define :web do |web|
    web.vm.provider "docker" do |d|
      d.build_dir = "./web"
      d.link "lb:lb"
    end
    web.vm.network :forwarded_port, guest: 80, host: HTTP_PORT
  end

  3.times do |i|
    config.vm.define "app#{i}", autostart: false do |app|
      app.vm.provider "docker" do |d|
        d.build_dir = "./app"
        d.env = {
          "SERF_NODE_NAME" => "app#{i}"
        }
        d.name = "app#{i}"
        d.link "db:db"
        d.link "lb:lb"
      end
    end
  end
end
