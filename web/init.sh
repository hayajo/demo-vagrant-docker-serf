#!/bin/sh

/usr/sbin/sshd -D &

nginx_conf=/etc/nginx/sites-enabled/default
cp /vagrant/web/default $nginx_conf
perl -i -pale 's/\{\{\s*(.+?)\s*\}\}/$ENV{$1}/eg' $nginx_conf

/usr/sbin/nginx

tail -F /var/log/nginx/access.log
