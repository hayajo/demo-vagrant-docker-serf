#!/bin/sh

/usr/sbin/sshd -D &
/usr/sbin/haproxy -f /etc/haproxy/haproxy.cfg -D -p "/var/run/haproxy.pid"
/usr/bin/serf agent -config-file=`dirname $0`/serf/lb.json
